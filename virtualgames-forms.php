<?php
/**
 * @wordpress-plugin
 * Plugin Name: Virtual Games Forms
 * Plugin URI:  https://jensengroup.ca/
 * Description: Register users from Ninja forms results
 * Version:     2022.3
 * Author:      Jensen Group
 * License:     GPL-2.0+
 * License URI: http:// www.gnu.org/licenses/gpl-2.0.txt
 * @package   jg-Gutenberg
 *
 * @author    Jensen Group
 * @copyright 2021 Jensen Group
 * @license   GPL-2.0+
 */

$files = ['customizer', 'assets',
    'cpt-user-sport',
    'ninja/setup', 'ninja/helpers', 'ninja/config',
    'ninja/fields/password',
    'ninja/action/family', 'ninja/action/guestbook', 'ninja/action/password', 'ninja/action/register', 'ninja/action/sport',
    'ninja/default/family', 'ninja/default/guestbook', 'ninja/default/profile', 'ninja/default/sport',
];
foreach ( $files as $file ) {
    $file = plugin_dir_path( __FILE__ ) . 'app/' . $file . '.php';

    if ( file_exists( $file ) ) {
        require $file;
    }
}

add_filter( 'ninja_forms_register_fields', 'register_fields' );
function register_fields( $fields ) {
    $fields['jg_password'] = new NF_Password();

    return $fields;
}
