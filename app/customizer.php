<?php
namespace jg\Plugin\User;

add_action( 'customize_register', function ( \WP_Customize_Manager $wp_customize ) {
	$wp_customize->add_section(
		'jg_info',
		[
			'title'       => __( 'Virtual Games', 'jg-forms' ),
			'description' => __( 'Contact info and social media links', 'jg-forms' ),
			'priority'    => 150,
		]
	);

	$wp_customize->add_setting(
		'jg_signin',
		[
		]
	);
	$wp_customize->add_control(
		'jg_signin',
		[
			'label'    => esc_html__( 'Sign In Page', 'jg-forms' ),
			'section'  => 'jg_info',
			'type'     => 'dropdown-pages',
			'priority' => 99,
		]
	);

	$wp_customize->add_setting(
		'jg_registration_start',
		[
		]
	);
	$wp_customize->add_control(
		'jg_registration_start',
		[
			'label'    => esc_html__( 'Registration Form Page', 'jg-forms' ),
			'section'  => 'jg_info',
			'type'     => 'dropdown-pages',
			'priority' => 99,
		]
	);

	$wp_customize->add_setting(
		'jg_registration_complete',
		[
		]
	);
	$wp_customize->add_control(
		'jg_registration_complete',
		[
			'label'    => esc_html__( 'Registration Completion Page', 'jg-forms' ),
			'section'  => 'jg_info',
			'type'     => 'dropdown-pages',
			'priority' => 99,
		]
	);

	$wp_customize->add_setting(
		'jg_profile',
		[
		]
	);
	$wp_customize->add_control(
		'jg_profile',
		[
			'label'    => esc_html__( 'Profile Page', 'jg-forms' ),
			'section'  => 'jg_info',
			'type'     => 'dropdown-pages',
			'priority' => 99,
		]
	);
} );