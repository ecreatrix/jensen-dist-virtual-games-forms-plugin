<?php
namespace jg\Plugin\User;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( UserSport::class ) ) {
    class UserSport {
        public function __construct() {
            add_action( 'init', [$this, 'register_post_type'] );
        }

        public function register_post_type() {
            $labels = [
                'name'           => _x( 'User Activities', 'Post Type General Name', 'jg-forms' ),
                'singular_name'  => _x( 'User Activity', 'Post Type Singular Name', 'jg-forms' ),
                'menu_name'      => __( 'User Activities', 'jg_users' ),
                'name_admin_bar' => __( 'Activity', 'jg_users' ),
                //'archives'       => __( 'User Activities Archives',  'jg-forms' ),
                'attributes'     => __( 'User Activities Attributes', 'jg-forms' ),
            ];

            $args = [
                'label'               => __( 'User Activitiess', 'jg-forms' ),
                'description'         => __( 'Virtual Game Activities', 'jg-forms' ),
                'labels'              => $labels,
                'supports'            => ['title', 'editor', 'custom-fields', 'page-attributes', 'author'],
                'hierarchical'        => false,
                'public'              => true,
                'show_ui'             => true,
                'show_in_menu'        => true,
                'menu_position'       => 5,
                'menu_icon'           => 'dashicons-universal-access',
                'show_in_admin_bar'   => true,
                'show_in_nav_menus'   => true,
                'can_export'          => true,
                'has_archive'         => false,
                'exclude_from_search' => true,
                'publicly_queryable'  => true,
                'capability_type'     => 'page',
                'show_in_rest'        => true,
            ];
            register_post_type( 'user-sport', $args );
        }
    }

    new UserSport();
}
