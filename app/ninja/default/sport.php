<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
// TODO: fix checkbox, radio list and family repeater
if ( ! class_exists( NinjaDefaultsSport::class ) ) {
	class NinjaDefaultsSport {
		private $_action_settings;

		private $_field_ids;

		public function __construct( $action_settings, $field_ids ) {
			$this->_action_settings = $action_settings;
			$this->_field_ids       = $field_ids;

			add_filter( 'ninja_forms_render_options', [$this, 'render_options'], 10, 2 );
		}

		public function render_options( $options, $field_array ) {
			if ( 'jg-user-post-sport' === $this->_action_settings['type'] ) {
				$field_key  = $field_array['key'];
				$action_key = $this->_field_ids[$field_key]['action_key'];

				$modify_id = NinjaHelpers::get_modify_id( 'activity', $_GET['type'], $_GET['id'] );

				switch ( $action_key ) {
					case 'member' === $action_key:
						$user_id       = NinjaHelpers::get_user_id();
						$family        = get_user_meta( $user_id, 'family', true );
						$modify_member = get_post_meta( $modify_id, 'member', true );

						$args = [
							'author'         => $user_id,
							'post_type'      => 'user-sport',
							'orderby'        => 'post_date',
							'order'          => 'ASC',
							'posts_per_page' => -1, // no limit
						];

						$options = [[
							'label'    => 'Select family member',
							'value'    => '',
							'selected' => false,
						]];
						foreach ( $family as $key => $member ) {
							if ( '' === $key ) {
								continue;
							}

							$option = [
								'label'    => $member['firstname'],
								'value'    => $key,
								'selected' => false,
							];

							if ( $modify_id && $modify_member && $key === $modify_member ) {
								$options[0]['selected'] = false;
								$option['selected']     = true;
							}

							$options[$member['firstname']] = $option;
						}
						ksort( $options );

						break;
					case 'activity' === $action_key:
						$modify_sport = (int) get_post_meta( $modify_id, 'activity', true );

						$args = [
							'post_type'   => 'sport',
							'numberposts' => -1,
							'order'       => 'ASC',
							'orderby'     => 'post_title',
							//'tax_query'   => [
							//	[
							//		'taxonomy' => 'social-media',
							//		'operator' => 'NOT EXISTS'
							//	]
							//]
						];

						$posts = get_posts( $args );

						$options = [[
							'label'    => 'Select an activity',
							'value'    => '',
							'selected' => false,
						]];

						foreach ( $posts as $post ) {
							$post_id = $post->ID;
							$terms   = get_the_terms( $post, 'sport_tax' );
							$slug    = $terms[0]->name . ' - ' . $post->post_title;

							if ( 'social-media' === $terms[0]->slug ) {
								continue;
							}

							$option = [
								'label'    => $slug,
								'value'    => $post_id,
								'selected' => false,
							];

							if ( $modify_id && $modify_sport && $modify_sport === $post_id ) {
								$options[0]['selected'] = false;
								$option['selected']     = true;
							}

							$options[$slug] = $option;
						}
						ksort( $options );

						break;
				}
			}

			return $options;
		}
	}
}
