<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
// TODO: fix checkbox, radio list and family repeater
if ( ! class_exists( NinjaDefaultsGuestbook::class ) ) {
	class NinjaDefaultsGuestbook {
		private $_action_settings;

		private $_field_ids;

		public function __construct( $action_settings, $field_ids ) {
			$this->_action_settings = $action_settings;
			$this->_field_ids       = $field_ids;

			add_filter( 'ninja_forms_render_default_value', [$this, 'pre_populate_field_data'], 10, 3 );
		}

		public function pre_populate_field_data( $default_value, $field_type, $field_settings ) {
			$field_key  = $field_settings['key'];
			$action_key = $this->_field_ids[$field_key]['action_key'];

			$user_id = NinjaHelpers::get_user_id();

			$modify_id = NinjaHelpers::get_modify_id( 'guestbook', $_GET['type'], $_GET['id'] );

			if ( 'jg-user-post-guestbook' === $this->_action_settings['type'] && $action_key && $modify_id ) {
				$post = get_post( $modify_id );
				switch ( $action_key ) {
					case 'name':
						$default_value = $post->post_title;
						break;
					case 'comment':
						$default_value = $post->post_content;
						break;
				}
			}

			return $default_value;
		}
	}
}
