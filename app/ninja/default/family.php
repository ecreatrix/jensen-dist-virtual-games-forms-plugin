<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( NinjaDefaultsFamily::class ) ) {
	class NinjaDefaultsFamily {
		private $_action_settings;

		private $_field_ids;

		public function __construct( $action_settings, $field_ids ) {
			$this->_action_settings = $action_settings;
			$this->_field_ids = $field_ids;

			add_filter( 'ninja_forms_render_default_value', [$this, 'pre_populate_field_data'], 10, 3 );
			add_filter( 'ninja_forms_render_options', [$this, 'render_options'], 10, 2 );

			add_filter( 'ninja_forms_after_upgrade_settings', [$this, 'upgrade_field_settings'] );
		}

		public function pre_populate_field_data( $default_value, $field_type, $field_settings ) {
			$field_key = $field_settings['key'];
			$action_key = $this->_field_ids[$field_key]['action_key'];

			$modify_id = NinjaHelpers::get_modify_id( 'family', $_GET['type'], $_GET['id'] );

			if ( 'jg-family-member' === $this->_action_settings['type'] && $action_key && $modify_id ) {
				$user_id = NinjaHelpers::get_user_id();
				$family = get_user_meta( $user_id, 'family', true );
				$member = $family[$modify_id];

				switch ( $action_key ) {
					case 'family_first_name':
						$default_value = $member['firstname'];
						break;
					case 'family_last_name':
						$default_value = $member['lastname'];
						break;
					case 'family_dob':
						$default_value = $member['dob'];
						break;
					case 'family_shirt_size':
						$default_value = $member['shirt_size'];
						break;
				}
			}

			return $default_value;
		}

		// Set select/checkmarks to saved user value
		public function render_options( $options, $field_settings ) {
			$field_key = $field_settings['key'];
			$action_key = $this->_field_ids[$field_key]['action_key'];

			$member_id = NinjaHelpers::get_modify_id( 'family', $_GET['type'], $_GET['id'] );

			if ( false !== $modify_id && 'family_shirt_size' === $action_key ) {
				$user_id = NinjaHelpers::get_user_id();

				$family = get_user_meta( $user_id, 'family', true );

				if ( ! $family || ! array_key_exists( $member_id, $family ) ) {
					return $options;
				}

				$member = $family[$member_id];
				$default_value = $member['shirt_size'];

				foreach ( $options as $key => $option ) {
					if ( $option['value'] && $default_value === $option['value'] ) {
						$options[0]['selected'] = false;
						$options[$key]['selected'] = 1;
					}
				}
			}

			return $options;
		}
	}
}
