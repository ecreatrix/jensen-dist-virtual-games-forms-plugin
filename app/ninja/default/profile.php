<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( NinjaDefaultsProfile::class ) ) {
	class NinjaDefaultsProfile {
		private $_action_settings;

		private $_field_ids;

		public function __construct( $action_settings, $field_ids ) {
			$this->_action_settings = $action_settings;
			$this->_field_ids = $field_ids;

			add_filter( 'ninja_forms_render_default_value', [$this, 'pre_populate_field_data'], 10, 3 );
			add_filter( 'ninja_forms_render_options', [$this, 'render_options'], 10, 2 );
		}

		public function pre_populate_field_data( $default_value, $field_type, $field_settings ) {
			$field_key = $field_settings['key'];
			$action_key = $this->_field_ids[$field_key]['action_key'];

			$modify_id = NinjaHelpers::get_modify_id( 'profile', $_GET['type'], $_GET['id'] );

			if ( 'jg-register-user' === $this->_action_settings['type'] && $action_key ) {
				$user_id = NinjaHelpers::get_user_id();
				$user_data = get_userdata( $user_id );

				switch ( $action_key ) {
					case 'username':
						$default_value = $user_data->user_login;
						break;
					case 'user_first_name':
						$default_value = $user_data->first_name;
						break;
					case 'user_last_name':
						$default_value = $user_data->last_name;
						break;
					case 'nickname':
						$default_value = $user_data->user_nicename;
						break;
					case 'email':
					case 'email_confirmation':
						$default_value = $user_data->user_email;
						break;
					case 'password':
						$default_value = false;
						break;
					default:
						if ( 'html' !== $field_type ) {
							$meta = get_user_meta( $user_id, $action_key, true );

							if ( $meta ) {
								$default_value = $meta;
							}
						}
						break;
				}
			}

			return $default_value;
		}

		// Set select/checkmarks to saved user value
		public function render_options( $options, $field_array ) {
			$user_id = NinjaHelpers::get_user_id();

			if ( 'jg-register-user' === $this->_action_settings['type'] ) {
				$field_key = $field_array['key'];
				$action_key = $this->_field_ids[$field_key]['action_key'];

				$user_meta = get_user_meta( $user_id, $action_key, true );

				if ( ! $user_meta ) {
					return $options;
				}

				foreach ( $options as $key => $option ) {
					if ( $option['value'] && $user_meta === $option['value'] ) {
						$options[$key]['selected'] = 1;
					} else {
						$options[$key]['selected'] = 0;
					}

				}
			}

			return $options;
		}
	}
}
