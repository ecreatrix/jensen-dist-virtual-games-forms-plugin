<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( NinjaActionGuestbook::class ) && class_exists( 'NF_Abstracts_Action' ) ) {
    class NinjaActionGuestbook extends \NF_Abstracts_Action {
        protected $_name = 'jg-user-post-guestbook';

        protected $_priority = '10';

        protected $_tags = [];

        protected $_timing = 'normal';

        public function __construct() {
            parent::__construct();

            $this->_nicename = __( 'Post User Comment', 'jg-forms' );

            add_action( 'admin_init', [$this, 'init_settings'] );
        }

        public function init_settings() {
            $settings = NinjaConfig::post_guestbook();

            $this->_settings = array_merge( $this->_settings, $settings );
        }

        public function process( $action_values, $form_id, $data ) {
            $action_settings = NinjaHelpers::get_action_settings( $form_id, $action_values['id'], $this->_settings );
            $field_ids       = NinjaHelpers::get_field_ids( $form_id, $action_settings, false );
            $user_id         = NinjaHelpers::get_user_id();

            $name    = $action_values['name'];
            $comment = $action_values['comment'];

            $post_data = [
                'post_title'   => $name,
                'post_content' => $comment,
                'post_type'    => 'guestbook',
                'post_status'  => 'publish',
                'post_author'  => $user_id,
            ];

            $query_id = $action_values['guestbook_query_id'];
            if ( $query_id ) {
                $post_data['ID'] = $query_id;
                $post_id         = wp_update_post( $post_data );
            } else {
                $post_id = wp_insert_post( $post_data );
            }

            $post_id      = $action_values['guestbook_post_id'];
            $profile_page = \jg\Theme\Helpers::theme_page( $post_id, 'profile' );
            if ( is_user_logged_in() && $profile_page['current_page'] ) {
                $data['actions']['redirect'] = $profile_page['permalink'] . '?type=guestbook';
            } else {
                $post_id = $action_values['guestbook_post_id'];

                $data['actions']['redirect'] = get_permalink( $post_id );
            }

            return $data;
        }
    }
}
