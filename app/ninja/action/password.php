<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( NinjaActionPassword::class ) && class_exists( 'NF_Abstracts_Action' ) ) {
    class NinjaActionPassword extends \NF_Abstracts_Action {
        protected $_name = 'jg-password';

        protected $_nicename = 'Update password';

        protected $_priority = '10';

        protected $_tags = [];

        protected $_timing = 'normal';

        public function __construct() {
            parent::__construct();

            add_action( 'admin_init', [$this, 'init_settings'] );
        }

        public function init_settings() {
            $settings = NinjaConfig::update_password();

            $this->_settings = array_merge( $this->_settings, $settings );
        }

        public function process( $action_values, $form_id, $data ) {
            $action_settings = NinjaHelpers::get_action_settings( $form_id, $action_values['id'], $this->_settings );
            $field_ids       = NinjaHelpers::get_field_ids( $form_id, $action_settings, false );

            $new_password     = $action_values['new_password'];
            $current_password = $action_values['current_password'];
            $user_id          = $action_values['query_user_id'] ? $action_values['query_user_id'] : $action_values['wp_user_id'];
            $query_post_id    = $action_values['query_post_id'] ? $action_values['query_post_id'] : $action_values['wp_post_id'];

            $userdata = get_user_by( 'id', $user_id );
            $username = $userdata->user_login;

            $password_check = wp_check_password( $current_password, $userdata->user_pass, $user_id );

            if ( ! $password_check ) {
                $data['errors']['fields'][$field_ids['current_password']['field_id'] . '_1'] = [
                    'message' => 'Wrong password. Please enter your current password.',
                    'slug'    => 'jg-forms',
                ];

                return $data;
            }

            $set = NinjaHelpers::update_password( $user_id, $username, $new_password );

            $profile_page      = \jg\Theme\Helpers::theme_page( $query_post_id, 'profile' );
            $registration_page = \jg\Theme\Helpers::theme_page( $query_post_id, 'registration_start' );

            if ( is_user_logged_in() && $profile_page['current_page'] ) {
                $data['actions']['redirect'] = $profile_page['permalink'];
            } else if ( is_user_logged_in() && $registration_page['current_page'] ) {
                $data['actions']['redirect'] = $profile_page['permalink'];
            }

            return $data;
        }
    }
}
