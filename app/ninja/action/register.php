<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( NinjaActionRegister::class ) && class_exists( 'NF_Abstracts_Action' ) ) {
    class NinjaActionRegister extends \NF_Abstracts_Action {
        protected $_name = 'jg-register-user';

        protected $_priority = '10';

        protected $_tags = [];

        protected $_timing = 'normal';

        public function __construct() {
            parent::__construct();

            $this->_nicename = __( 'Register User', 'user' );

            add_action( 'admin_init', [$this, 'init_settings'] );
        }

        public function init_settings() {
            $settings = NinjaConfig::register_user();

            $this->_settings = array_merge( $this->_settings, $settings );
        }

        public function process( $action_values, $form_id, $data ) {
            $action_settings = NinjaHelpers::get_action_settings( $form_id, $action_values['id'], $this->_settings );
            $field_ids       = NinjaHelpers::get_field_ids( $form_id, $action_settings, false );

            $post_id = $action_values['query_post_id'];

            $username        = $action_values['username'];
            $username        = sanitize_user( trim( esc_attr( $username ) ) );
            $username_exists = username_exists( $username );

            $email        = $action_values['email'];
            $email        = apply_filters( 'user_registration_email', esc_attr( $email ) );
            $email_exists = email_exists( $email );

            $user_id = NinjaHelpers::get_user_id();
            if ( $user_id ) {
                $current_user = get_userdata( $user_id );

                $username_exists = $current_user->user_login !== $username && $username_exists;
                $email_exists    = $current_user->user_email !== $email && $email_exists;
            }

            if ( $username && $username_exists ) {
                $data['errors']['fields'][$field_ids['username']['field_id']] = [
                    'message' => NinjaHelpers::error_in_use( 'username' ),
                    'slug'    => 'jg-forms',
                ];
            }

            if ( $username && strlen( $username ) < 3 ) {
                $data['errors']['fields'][$field_ids['username']['field_id']] = [
                    'message' => 'Please choose a username that is at least <u>3</u> characters long.' . $username,
                    'slug'    => 'jg-forms',
                ];
            }

            if ( $email_exists ) {
                $data['errors']['fields'][$field_ids['email']['field_id']] = [
                    'message' => NinjaHelpers::error_in_use( 'email' ),
                    'slug'    => 'jg-forms',
                ];
            }

            if ( $username_exists || $email_exists ) {
                return $data;
            }

            $password = $action_values['password'];

            if ( ! empty( $password ) || $password ) {
                $password_failed = NinjaHelpers::password_security_check( $password );

                if ( $password_failed ) {
                    $data['errors']['fields'][$field_ids['password']['field_id']] = [
                        'message' => $password_failed,
                        'slug'    => 'jg-forms',
                    ];

                    return $data;
                }
            }

            $user_data = [
                'ID'         => $user_id,
                'first_name' => $action_values['user_first_name'],
                'last_name'  => $action_values['user_last_name'],
                'user_email' => $action_values['email'],
            ];

            if ( $action_values['username'] ) {
                $user_data['user_login'] = $action_values['username'];
            }

            if ( $action_values['nickname'] ) {
                $user_data['user_nicename'] = $action_values['nickname'];
            }

            if ( ! is_user_logged_in() || ! $user_id ) {
                $user_id = wp_create_user( $username, $password, $email );

                $user_data['ID'] = $user_id;

                if ( $user_id ) {
                    $user = new \WP_User( $user_id );
                    //$user->set_role( 'subscriber' );

                    // Password passes safety requirements
                    wp_set_password( $password, $user_id );
                }
            }

            $this->update_user( $post_id, $user_data, $user_id, $password );

            $this->set_meta_values( $field_ids, $action_values, $user_id );

            $profile_page      = \jg\Theme\Helpers::theme_page( $post_id, 'profile' );
            $registration_page = \jg\Theme\Helpers::theme_page( $post_id, 'registration_start' );

            if ( is_user_logged_in() && $profile_page['current_page'] ) {
                $data['actions']['redirect'] = $profile_page['permalink'];
            } else if ( is_user_logged_in() && $registration_page['current_page'] ) {
                $data['actions']['redirect'] = $profile_page['permalink'];
            }

            return $data;
        }

        public function set_meta_values( $field_ids, $action_values, $user_id ) {
            $meta_keys = array_filter( array_keys( $field_ids ) );
            foreach ( $meta_keys as $key ) {
                $value = $action_values[$key];

                if ( $value ) {
                    update_user_meta( $user_id, $key, $value );
                }
            }

            $family = NinjaHelpers::get_family_info( $action_values, $user_id, true );
            update_user_meta( $user_id, 'family', $family );

            $existing_points_total = $user_id && get_user_meta( $user_id, 'points_total', true ) ? get_user_meta( $user_id, 'points_total', true ) : 0;
            update_user_meta( $user_id, 'points_total', $existing_points_total );
        }

        public function update_user( $post_id, $user_data, $user_id, $password ) {
            wp_update_user( $user_data );

            // Password passes safety requirements
            if ( $password ) {
                wp_set_password( $password, $user_id );
            }

            if ( is_user_logged_in() ) {
                // Prevent user logout after password change
                wp_cache_delete( $user_id, 'users' );
                wp_cache_delete( $username, 'userlogins' );
                wp_logout();
                wp_signon( ['user_login' => $username, 'user_password' => $password] );
                wp_set_current_user( $user_id, $username );
                wp_set_auth_cookie( $user_id );
            }
        }
    }
}
