<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( NinjaActionFamilyMembers::class ) && class_exists( 'NF_Abstracts_Action' ) ) {
    class NinjaActionFamilyMembers extends \NF_Abstracts_Action {
        protected $_name = 'jg-family-member';

        protected $_priority = '10';

        protected $_tags = [];

        protected $_timing = 'normal';

        public function __construct() {
            parent::__construct();

            $this->_nicename = __( 'Add Family Member', 'jg-forms' );

            add_action( 'admin_init', [$this, 'init_settings'] );
        }

        public function init_settings() {
            $settings = NinjaConfig::update_family();

            $this->_settings = array_merge( $this->_settings, $settings );
        }

        public function process( $action_values, $form_id, $data ) {
            $action_settings = NinjaHelpers::get_action_settings( $form_id, $action_values['id'], $this->_settings );
            $field_ids       = NinjaHelpers::get_field_ids( $form_id, $action_settings, false );

            $user_id = NinjaHelpers::get_user_id();

            $family = NinjaHelpers::get_family_info( $action_values, $user_id );
            update_user_meta( $user_id, 'family', $family );

            $profile_page = get_permalink( get_theme_mod( 'jg_profile' ) ) . '?type=family';
            if ( $profile_page ) {
                $data['actions']['redirect'] = $profile_page;
            }

            return $data;
        }
    }
}