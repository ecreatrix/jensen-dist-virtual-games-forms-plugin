<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( NinjaActionSport::class ) && class_exists( 'NF_Abstracts_Action' ) ) {
	class NinjaActionSport extends \NF_Abstracts_Action {
		protected $_name = 'jg-user-post-sport';

		protected $_priority = '10';

		protected $_tags = [];

		protected $_timing = 'normal';

		public function __construct() {
			parent::__construct();

			$this->_nicename = __( 'Post User Activities', 'jg-forms' );

			add_action( 'admin_init', [$this, 'init_settings'] );
		}

		function get_already_complete_error( $firstname, $sport_title ) {
			return $firstname . ' has already completed the ' . $sport_title . ' challenge. Please try another one.';
		}

		function get_existing_post_info( $action_values, $sport_id ) {
			$query_id     = $action_values['sport_query_id'];
			$query_action = $action_values['sport_query_action'];
			$query_type   = $action_values['sport_query_type'];

			$existing = [
				'post_id'        => false,
				'sport_id'       => false,
				'sport_points'   => false,
				'member_id'      => false,
				'post_completed' => false,
			];

			if ( $query_action && 'edit' === $query_action
				&& $query_type && 'activity' === $query_type
				&& $query_id && null !== get_post( $query_id )
			) {
				// Post exists
				$existing_sport_id  = get_post_meta( $query_id, 'activity', true );
				$existing_member_id = get_post_meta( $query_id, 'member', true );

				$existing = [
					'post_id'        => $query_id,
					'post_completed' => get_post_meta( $query_id, 'completed', true ) === 'Complete',
					'sport_id'       => $existing_sport_id,
					'sport_points'   => get_post_meta( $query_id, 'points_earned', true ),
				];

				if ( $existing_member_id ) {
					$existing['member_id'] = $existing_member_id;
				}
			}

			return $existing;
		}

		function get_updated_info( $sport_id, $user_id, $member_id, $existing, $completed ) {
			$update_sport = $existing['sport_id'] === $sport_id;

			$sport        = get_post( $sport_id );
			$sport_points = get_post_meta( $sport_id, 'jg_sport_points', true );

			$user_points = get_user_meta( $user_id, 'points_total', true );
			$family_name = get_user_meta( $user_id, 'nickname', true );
			$family      = get_user_meta( $user_id, 'family', true );

			$member = $family[$member_id];

			$info = [
				'post_title'    => $user_id . ' - ' . $member['firstname'] . ' - ' . $sport->post_title,
				'user_points'   => $user_points,
				'points_earned' => '0',
				'family'        => $family,
				'unavailable'   => false,
			];

			$member_sport_key = $this->member_sport_key( $sport_id, $member['completed_sports'] );

			$update_post_same_sport = $this->update_post_same_sport( $sport_id, $existing );
			if ( $member_sport_key && ! $update_post_same_sport ) {
				// User has completed sport in the past, error
				$info['unavailable'] = $this->get_already_complete_error( $member['firstname'], $sport->post_title );

				return $info;
			}

			$existing_member_id        = $existing['member_id'];
			$existing_post_member      = $family[$existing_member_id];
			$existing_member_sport_key = $this->member_sport_key( $existing['sport_id'], $existing_post_member['completed_sports'] );
			// Post exists, update user
			if ( $existing_member_sport_key ) {
				if ( $existing['post_completed'] ) {
					$existing_post_member['points'] = $existing_post_member['points'] - $existing['sport_points'];
					$user_points                    = $user_points - $existing['sport_points'];
				}

				unset( $existing_post_member['completed_sports'][$existing_member_sport_key] );

				if ( $existing_member_id === $member_id ) {
					$member = $existing_post_member;
				} else {
					$family[$existing_member_id] = $existing_post_member;
				}
			}

			$points_earned = 0;
			if ( 'Complete' === $completed ) {
				$member['points'] = $member['points'] + $sport_points;
				$user_points      = $user_points + $sport_points;
				$points_earned    = $sport_points;
			}

			$family[$member_id]    = $member;
			$info['user_points']   = $user_points;
			$info['points_earned'] = $points_earned;
			$info['family']        = $family;

			return $info;
		}

		public function init_settings() {
			$settings = NinjaConfig::post_sport();

			$this->_settings = array_merge( $this->_settings, $settings );
		}

		function member_sport_key( $sport_id, $member_sports ) {
			$member_completed_sport_id = array_search( $sport_id, $member_sports );

			if ( false === $member_completed_sport_id ) {
				// Selected member has never completed selected sport
				return false;
			}

			return $member_completed_sport_id;
		}

		public function process( $action_values, $form_id, $data ) {
			$action_settings = NinjaHelpers::get_action_settings( $form_id, $action_values['id'], $this->_settings );
			$field_ids       = NinjaHelpers::get_field_ids( $form_id, $action_settings, false );

			$user_id = NinjaHelpers::get_user_id();

			$sport_id  = $action_values['activity'];
			$member_id = $action_values['member'];

			$version_id = $action_values['version'];

			if ( 'main' === $version_id ) {
				$version_id = '';
			}

			$completed = 'Checked' === $action_values['completed'] ? 'Complete' : 'Incomplete';

			$existing = $this->get_existing_post_info( $action_values, $sport_id );

			$updated     = $this->get_updated_info( $sport_id, $user_id, $member_id, $existing, $completed );
			$family      = $updated['family'];
			$user_points = $updated['user_points'];

			if ( $updated['unavailable'] ) {
				// User has completed sport in the past, error
				$data = $this->send_error( $data, $field_ids, 'activity', $updated['unavailable'], $version_id );

				return $data;
			}

			$post_data = [
				'post_title'   => $updated['post_title'],
				'post_content' => '',
				'post_type'    => 'user-sport',
				'post_status'  => 'publish',
				'post_author'  => $user_id,
			];

			if ( $existing['post_id'] ) {
				// Post exists, update
				$post_data['ID'] = $existing['post_id'];

				$post_id = wp_update_post( $post_data );
			} else {
				// Member has not completed sport so add sport to member info and add post
				$post_id = wp_insert_post( $post_data );
			}

			if ( ! $post_id ) {
				$data = $this->send_error( $data, $field_ids, 'activity', 'An error has occured. Please contact us for support.', $version_id );

				return $data;
			}

			$family[$member_id]['completed_sports'][$post_id] = $sport_id;

			update_user_meta( $user_id, 'family', $family );
			update_user_meta( $user_id, 'points_total', $user_points );

			update_post_meta( $post_id, 'activity', $sport_id );
			update_post_meta( $post_id, 'member', $member_id );
			update_post_meta( $post_id, 'completed', $completed );
			update_post_meta( $post_id, 'media', $action_values['media'] );
			update_post_meta( $post_id, 'points_earned', $updated['points_earned'] );

			$current_post_id = $action_values['sport_post_id'];

			$profile_page = \jg\Theme\Helpers::theme_page( $current_post_id, 'profile' );

			if ( is_user_logged_in() && $profile_page['current_page'] ) {
				$data['actions']['redirect'] = $profile_page['permalink'] . '?type=activity';
			} else {
				$data['actions']['redirect'] = get_permalink( $current_post_id );
			}

			return $data;
		}

		function send_error( $data, $field_ids, $key, $message, $version_id ) {
			$data['errors']['fields'][$field_ids[$key]['field_id'] . $version_id] = [
				'message' => $message,
				'slug'    => 'jg-forms',
			];
			return $data;
		}

		function update_post_same_sport( $sport_id, $existing ) {
			if ( $existing['member_id'] ) {
				$existing_post_member = $family[$existing['member_id']];

				$existing_member_sport_key = $this->member_sport_key( $existing['sport_id'], $existing_post_member['completed_sports'] );

				return $existing['sport_id'] && $existing['sport_id'] === $sport_id;
			}

			return false;
		}
	}
}
