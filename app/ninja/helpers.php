<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( NinjaHelpers::class ) ) {
    class NinjaHelpers {
        public function __construct() {}
        static function calculate_age( $dob ) {
            $birthyear = date( 'Y', strtotime( $dob ) );
            $age       = date( 'Y' ) - $birthyear;

            $age_category = '0-12';
            if ( $age >= 13 && $age <= 19 ) {
                $age_category = '13-19';
            } else if ( $age >= 20 ) {
                $age_category = '20+';
            }

            return [
                'age'          => $age,
                'age_category' => $age_category,
            ];
        }

        static function error_in_use( $field = 'email' ) {
            return __( 'Unfortunately this ' . $field . ' is already used by somebody else. Please try another one.', 'jg-forms' );
        }

        static function finish_registration( $form_id, $user_submission_key ) {
            $form             = Ninja_Forms()->form( $form_id );
            $form_submissions = $form->get_subs();

            $actions   = Ninja_Forms()->form( $form_id )->get_actions();
            $field_ids = false;

            // Ninja form not found
            if ( 'reset' !== $user_submission_key && ( ! is_array( $form_submissions ) || empty( $form_submissions ) ) ) {
                return false;
            }

            foreach ( $actions as $action ) {
                $action_type     = $action->get_setting( 'type' );
                $action_settings = $action->get_settings();

                if ( 'jg-register-user' === $action_type ) {
                    $field_ids = self::get_field_ids( $form_id, $action_settings, false );
                }
            }

            if ( ! $field_ids ) {
                return false;
            }
            foreach ( $form_submissions as $submission ) {
                $fields = $submission->get_field_values();
                $email  = $fields[$field_ids['email']['field_key']];
                $email  = preg_replace( '/^([^,]*).*$/', '$1', $email );

                $key = $fields[$field_ids['reg_key']['field_key']];

                $user    = get_user_by( 'email', $email );
                $user_id = $user->ID;
                //echo '<br>';

                if ( 'reset' === $user_submission_key ) {
                    var_dump( $user_id . ' ' . $email );
                    //self::refresh_users( $fields, $field_ids, $user );
                    var_dump( '<br><br>' );
                } else if ( $key == $user_submission_key ) {
                    //var_dump( $user_id . ' ' . $key );
                    if ( ! $user ) {
                        return false;
                    }

                    if ( ! in_array( 'administrator', (array) $user->roles ) ) {
                        $user->set_role( 'author' );
                        $user->set_role( 'author' );
                    }
                }
            }

            return true;
        }

        static function get_action_settings( $form_id, $action_id, $config ) {
            $form            = Ninja_Forms()->form( $form_id );
            $action          = $form->get_action( $action_id );
            $action_settings = $action->get_settings( array_keys( $config ) );

            return $action_settings;
        }

        public function get_family_info( $action_settings, $user_id, $member_main = false ) {
            $family = get_user_meta( $user_id, 'family', true );
            if ( ! $family || ! is_array( $family ) ) {
                $family = [];
            }

            $dob       = $action_settings['family_dob'] ? $action_settings['family_dob'] : $action_settings['dob'];
            $age_info  = NinjaHelpers::calculate_age( $dob );
            $firstname = $action_settings['family_first_name'] ? $action_settings['family_first_name'] : $action_settings['user_first_name'];
            $lastname  = $action_settings['family_last_name'] ? $action_settings['family_last_name'] : $action_settings['user_last_name'];

            $slug = sanitize_title( $firstname . ' ' . $lastname );

            //$completed_sports = $family[$slug]

            $member = [
                'points'           => 0,
                'completed_sports' => [],
                'firstname'        => $firstname,
                'lastname'         => $lastname,
                'dob'              => $dob,
                'age'              => $age_info['age'],
                'age_category'     => $age_info['age_category'],
                'shirt_size'       => $action_settings['family_shirt_size'] ? $action_settings['family_shirt_size'] : $action_settings['shirt_size'],
            ];

            if ( $member_main ) {
                $slug = 'profile';

                $member['points']           = $family[$slug]['points'];
                $member['completed_sports'] = $family[$slug]['completed_sports'];
            }

            $modify_type   = $action_settings['family_query_type'];
            $modify_id     = $action_settings['family_query_id'];
            $modify_action = $action_settings['family_query_action'];

            if ( $modify_action && 'family' === $modify_type
                && $modify_action && 'edit' === $modify_action
                && $modify_id && array_key_exists( $modify_id, $family )
            ) {
                $slug = $modify_id;

                $member['points']           = $family[$slug]['points'];
                $member['completed_sports'] = $family[$slug]['completed_sports'];
            }

            $family[$slug] = $member;

            return $family;
        }

        //Get the field key of each field in the fields array.
        static function get_field_ids( $form_id, $action_settings, $by_field = true ) {
            $form        = Ninja_Forms()->form( $form_id );
            $action_keys = self::strip_merge_tags( $action_settings );

            $fields    = $form->get_fields();
            $field_ids = [];
            foreach ( $fields as $field ) {
                $field_key = $field->get_setting( 'key' );
                $field_id  = $field->get_id();

                $action_key = $action_keys[$field_key];

                if ( $by_field ) {
                    $field_ids[$field_key] = [
                        'field_id'   => $field_id,
                        'action_key' => $action_key,
                    ];
                } else {
                    $field_ids[$action_key] = [
                        'field_id'  => $field_id,
                        'field_key' => $field_key,
                    ];
                }
            }

            return $field_ids;
        }

        public static function get_modify_id( $current_type, $modify_type, $modify_id ) {
            $modify_type = $modify_type ? $modify_type : false;
            $modify_id   = isset( $modify_id ) ? $modify_id : false;

            if ( $current_type === $modify_type && false !== $modify_id ) {
                return $modify_id;
            }

            return false;
        }

        static function get_user_id() {
            global $current_user;
            get_currentuserinfo();
            $user_id = $current_user->ID;

            return $user_id;
        }

        static function password_security_check( $password ) {
            $password_regex = '/^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/';
            if ( preg_match( $password_regex, $password ) ) {
                // Password passes safety requirements
                return false;
            }

            $requirements = [
                [
                    'regex'   => '/^.*(?=.{8,}).*$/',
                    'message' => 'at least 8 characters',
                ], [
                    'regex'   => '/^.*((?=.*[A-Z]){1}).*$/',
                    'message' => 'at least one capital letter',
                ], [
                    'regex'   => '/^.*((?=.*[a-z]){1}).*$/',
                    'message' => 'at least one lower-case letter',
                ], [
                    'regex'   => '/^.*(?=.*\d).*$/',
                    'message' => 'at least one digit',
                ], [
                    'regex'   => '/^.*((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1}).*$/',
                    'message' => 'at least one special symbol like from this set: !@#$%^&*()\-_=+{};:,<.>]',
                ],
            ];
            $message = [];

            foreach ( $requirements as $req ) {
                $class = 'invalid';
                if ( preg_match( $req['regex'], $password ) ) {
                    $class = 'valid';
                }

                $message[] = '<li class="' . $class . '">' . $req['message'] . '</li>';
            }

            return '<ul class="password-requirements">' . implode( $message ) . '</ul>';
        }

        // Update all accounts if admin
        public static function refresh_users( $fields, $field_ids, $user ) {
            if ( ! is_user_logged_in() ) {
                return false;
            }

            $user_id      = self::get_user_id();
            $current_user = get_user_by( 'ID', $user_id );
            var_dump( $user );

            if ( in_array( 'administrator', (array) $current_user->roles ) && $user ) {
                $user_id   = $user->ID;
                $user_meta = get_user_meta( $user_id );
                var_dump( $user_id );

                if ( in_array( 'subscriber', (array) $user->roles ) || in_array( 'author', (array) $user->roles ) ) {
                    $action_values = [];
                    foreach ( $field_ids as $key => $field_id ) {
                        $value               = $fields[$field_id['field_key']];
                        $action_values[$key] = $value;

                        if ( null === $user_meta[$key] || ! is_array( $user_meta[$key] ) || '' === $user_meta[$key][0] ) {
                            update_user_meta( $user_id, $key, $value );
                        }
                    }

                    var_dump( '<br>' );
                    $family = self::get_family_info( $action_values, $user_id, true );
                    var_dump( $family );
                    if ( array_key_exists( 'main', $family ) ) {
                        $family['profile'] = $family['main'];
                        unset( $family['main'] );
                    }
                    var_dump( $family );
                    update_user_meta( $user_id, 'family', $family );

                    return true;
                }
            }

            return false;
        }

        //Removes the merge tag formatting
        public static function strip_merge_tags( $action_settings ) {
            $settings = [];

            foreach ( $action_settings as $key => $value ) {
                $settings_value            = str_replace( '{field:', '', $value );
                $settings_value            = str_replace( '}', '', $settings_value );
                $settings[$settings_value] = $key;
            }

            return $settings;
        }

        static function theme_page( $post_id, $theme_page ) {
            if ( ! method_exists( '\jg\Theme\Helpers', 'theme_page' ) ) {
                return false;
            }

            return \jg\Theme\Helpers::theme_page( $post_id, $theme_page );
        }

        public function update_password( $user_id, $username, $password ) {
            $set = wp_set_password( $password, $user_id );

            if ( is_user_logged_in() ) {
                // Prevent user logout after password change
                wp_cache_delete( $user_id, 'users' );
                wp_cache_delete( $username, 'userlogins' );
                wp_logout();
                wp_signon( ['user_login' => $username, 'user_password' => $password] );
                wp_set_current_user( $user_id, $username );
                //wp_set_auth_cookie( $user_id );
            }

            return $set;
        }
    }
}