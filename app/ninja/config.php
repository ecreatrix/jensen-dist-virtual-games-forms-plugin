<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( NinjaConfig::class ) ) {
    class NinjaConfig {
        public function __construct() {
            apply_filters( 'ninja_forms_register_user_settings', [$this, 'register_user'] );
            apply_filters( 'ninja_forms_post_sport_settings', [$this, 'post_sport'] );
            apply_filters( 'ninja_forms_family_settings', [$this, 'update_family'] );
            apply_filters( 'ninja_forms_post_guestbook_settings', [$this, 'post_guestbook'] );
            apply_filters( 'ninja_forms_update_password_settings', [$this, 'update_password'] );
        }

        public static function post_guestbook() {
            return [
                'name'                   => [
                    'name'        => 'name',
                    'type'        => 'field-select',
                    'label'       => __( 'Name', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'firstname',
                        'textbox',
                    ],
                ],
                'comment'                => [
                    'name'        => 'comment',
                    'type'        => 'field-select',
                    'label'       => __( 'Comment', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'textarea',
                        'textbox',
                    ],
                ],
                'guestbook_query_id'     => [
                    'name'        => 'guestbook_query_id',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Member ID', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'guestbook_query_action' => [
                    'name'        => 'guestbook_query_action',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Action', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'guestbook_query_type'   => [
                    'name'        => 'guestbook_query_type',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Member Type', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'guestbook_post_id'      => [
                    'name'        => 'guestbook_post_id',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Current Page', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
            ];
        }

        public static function post_sport() {
            return [
                'activity'           => [
                    'name'        => 'activity',
                    'type'        => 'field-select',
                    'label'       => __( 'Activity', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'listselect',
                    ],
                ],
                'completed'          => [
                    'name'        => 'completed',
                    'type'        => 'field-select',
                    'label'       => __( 'Completed', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'checkbox',
                    ],
                ],
                'member'             => [
                    'name'        => 'member',
                    'type'        => 'field-select',
                    'label'       => __( 'Family Member', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'listselect',
                    ],
                ],
                'media'              => [
                    'name'        => 'media',
                    'type'        => 'field-select',
                    'label'       => __( 'Video/Image', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'file_upload',
                    ],
                ],
                'sport_query_id'     => [
                    'name'        => 'sport_query_id',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Member ID', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'sport_query_action' => [
                    'name'        => 'sport_query_action',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Action', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'sport_query_type'   => [
                    'name'        => 'sport_query_type',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Member Type', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'sport_post_id'      => [
                    'name'        => 'sport_post_id',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Current Page', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'version'            => [
                    'name'        => 'version',
                    'type'        => 'field-select',
                    'label'       => __( 'Duplicate form version', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                    ],
                ],
            ];
        }

        public static function register_user() {
            return [
                'username'                   => [
                    'name'        => 'username',
                    'type'        => 'field-select',
                    'label'       => __( 'Username', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'textbox',
                    ],
                ],
                'email'                      => [
                    'name'        => 'email',
                    'type'        => 'field-select',
                    'label'       => __( 'Email', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'textbox',
                        'email',
                    ],
                ],
                'email_confirmation'         => [
                    'name'        => 'email_confirmation',
                    'type'        => 'field-select',
                    'label'       => __( 'Email Confirmation', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'confirm',
                    ],
                ],
                'user_first_name'            => [
                    'name'        => 'user_first_name',
                    'type'        => 'field-select',
                    'label'       => __( 'First Name', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'firstname',
                        'textbox',
                    ],
                ],
                'user_last_name'             => [
                    'name'        => 'user_last_name',
                    'type'        => 'field-select',
                    'label'       => __( 'Last Name', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'lastname',
                        'textbox',
                    ],
                ],
                'password'                   => [
                    'name'        => 'password',
                    'type'        => 'field-select',
                    'label'       => __( 'Password', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'jg_password',
                    ],
                ],
                'dob'                        => [
                    'name'        => 'dob',
                    'type'        => 'field-select',
                    'label'       => __( 'Date of Birth', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'date',
                    ],
                ],
                'shirt_size'                 => [
                    'name'        => 'shirt_size',
                    'type'        => 'field-select',
                    'label'       => __( 'Shirt Size', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'listselect',
                    ],
                ],
                'street1'                    => [
                    'name'        => 'street1',
                    'type'        => 'field-select',
                    'label'       => __( 'Street', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'address',
                    ],
                ],
                'city1'                      => [
                    'name'        => 'city1',
                    'type'        => 'field-select',
                    'label'       => __( 'City', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'city',
                    ],
                ],
                'postal_code1'               => [
                    'name'        => 'postal_code1',
                    'type'        => 'field-select',
                    'label'       => __( 'Postal Code', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'textbox',
                    ],
                ],
                'province_territory1'        => [
                    'name'        => 'province_territory1',
                    'type'        => 'field-select',
                    'label'       => __( 'Province/Territory', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'listselect',
                    ],
                ],
                'indigenous_ancestry'        => [
                    'name'        => 'indigenous_ancestry',
                    'type'        => 'field-select',
                    'label'       => __( 'Indigenous Ancestry', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'listselect',
                    ],
                ],
                'iswo_region'                => [
                    'name'        => 'iswo_region',
                    'type'        => 'field-select',
                    'label'       => __( 'ISWO Region', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'listselect',
                    ],
                ],
                'avatar'                     => [
                    'name'        => 'avatar',
                    'type'        => 'field-select',
                    'label'       => __( 'Avatar', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'file_upload',
                    ],
                ],
                'accept_privacy'             => [
                    'name'        => 'accept_privacy',
                    'type'        => 'field-select',
                    'label'       => __( 'Accept Privacy Check', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'checkbox',
                    ],
                ],
                'accept_waivers'             => [
                    'name'        => 'accept_waivers',
                    'type'        => 'field-select',
                    'label'       => __( 'Accept Waivers Check', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'checkbox',
                    ],
                ],
                'accept_safety_requirements' => [
                    'name'        => 'accept_safety_requirements',
                    'type'        => 'field-select',
                    'label'       => __( 'Accept Safety Requirements Check', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'checkbox',
                    ],
                ],
                'query_id'                   => [
                    'name'        => 'query_id',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Member ID', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'query_action'               => [
                    'name'        => 'query_action',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Action', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'query_type'                 => [
                    'name'        => 'query_type',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Member Type', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'query_post_id'              => [
                    'name'        => 'query_post_id',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Current Page', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'reg_key'                    => [
                    'name'        => 'reg_key',
                    'type'        => 'field-select',
                    'label'       => __( 'Registration Key', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                    ],
                ],
            ];
        }

        public static function update_family() {
            return [
                'family_first_name'               => [
                    'name'        => 'family_first_name',
                    'type'        => 'field-select',
                    'label'       => __( 'First Name', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'firstname',
                        'textbox',
                    ],
                ],
                'family_last_name'                => [
                    'name'        => 'family_last_name',
                    'type'        => 'field-select',
                    'label'       => __( 'Last Name', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'lastname',
                        'textbox',
                    ],
                ],
                'family_dob'                      => [
                    'name'        => 'family_dob',
                    'type'        => 'field-select',
                    'label'       => __( 'Date of Birth', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'date',
                    ],
                ],
                'family_parent_or_legal_guardian' => [
                    'name'        => 'family_parent_or_legal_guardian',
                    'type'        => 'field-select',
                    'label'       => __( 'Parent/Guardian Check', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'listradio',
                        'checkbox',
                    ],
                ],
                'family_participation_permission' => [
                    'name'        => 'family_participation_permission',
                    'type'        => 'field-select',
                    'label'       => __( 'Participation Permission Check', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'listradio',
                        'checkbox',
                    ],
                ],
                'family_shirt_size'               => [
                    'name'        => 'family_shirt_size',
                    'type'        => 'field-select',
                    'label'       => __( 'Shirt Size', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'listselect',
                    ],
                ],
                'family_query_id'                 => [
                    'name'        => 'family_query_id',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Member ID', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'family_query_action'             => [
                    'name'        => 'family_query_action',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Action', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'family_query_type'               => [
                    'name'        => 'family_query_type',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Member Type', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
            ];
        }

        public static function update_password() {
            return [
                'current_password' => [
                    'name'        => 'current_password',
                    'type'        => 'field-select',
                    'label'       => __( 'Current Password', 'jg-forms' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'jg_password',
                    ],
                ],
                'new_password'     => [
                    'name'        => 'new_password',
                    'type'        => 'field-select',
                    'label'       => __( 'New Password', 'jg-forms' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'jg_password',
                    ],
                ],
                'query_post_id'    => [
                    'name'        => 'query_post_id',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Current Page', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                    ],
                ],
                'query_user_id'    => [
                    'name'        => 'query_user_id',
                    'type'        => 'field-select',
                    'label'       => __( 'Query User ID', 'jg-forms' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                    ],
                ],
            ];
        }
    }
}