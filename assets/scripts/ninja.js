/******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./virtualgames-forms/src/scripts/ninja/activity.jsx":
/*!***********************************************************!*\
  !*** ./virtualgames-forms/src/scripts/ninja/activity.jsx ***!
  \***********************************************************/
/***/ (function() {

jQuery(document).ready(function ($) {
  new ninjaFormsVersionController();
});
var submitSportIDs;
var token = 'jg-user-management';
var ninjaFormsVersionController = Marionette.Object.extend({
  initialize: function initialize() {
    var fieldsChannel = nfRadio.channel('fields');
    this.listenTo(fieldsChannel, 'render:view', this.setVersion);
  },
  setVersion: function setVersion(view) {
    var model = view.model;
    var key = model.get('key');
    var formID = model.get('formID');

    if ('version' === key) {
      var currentFormVersion = 'main';

      if (formID.indexOf('_') >= 0) {
        currentFormVersion = formID.substr(formID.indexOf('_'));
      }

      jQuery(view.el).find('.nf-element').val(currentFormVersion).trigger('change');
    }
  }
});

/***/ }),

/***/ "./virtualgames-forms/src/scripts/ninja/errors.jsx":
/*!*********************************************************!*\
  !*** ./virtualgames-forms/src/scripts/ninja/errors.jsx ***!
  \*********************************************************/
/***/ (function() {

jQuery(document).ready(function ($) {
  new ninjaFormsCustomController();
});
var submitSportIDs;
var token = 'jg-forms';
var ninjaFormsCustomController = Marionette.Object.extend({
  initialize: function initialize() {
    var fieldsChannel = nfRadio.channel('fields');
    this.listenTo(fieldsChannel, 'blur:field', this.removeErrorFields);
    this.listenTo(fieldsChannel, 'change:field', this.removeErrorFields);
    this.listenTo(fieldsChannel, 'keyup:field', this.removeErrorFields);
  },
  removeErrorFields: function removeErrorFields(el, model) {
    nfRadio.channel('fields').request('remove:error', model.get('id'), token);
  }
});

/***/ }),

/***/ "./virtualgames-forms/src/scripts/ninja/key.jsx":
/*!******************************************************!*\
  !*** ./virtualgames-forms/src/scripts/ninja/key.jsx ***!
  \******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! uuid */ "./node_modules/uuid/dist/esm-browser/v4.js");

jQuery(document).ready(function ($) {
  new ninjaFormsUniqueKey();
});
var keyField = 'registration_key';
var uniqueKey = (0,uuid__WEBPACK_IMPORTED_MODULE_0__["default"])();
var forms = false;
var ninjaFormsUniqueKey = Marionette.Object.extend({
  initialize: function initialize() {
    var radioFields = nfRadio.channel('fields');
    this.listenTo(radioFields, 'render:view', this.setValue);
  },
  setValue: function setValue(view) {
    var modelKey = view.model.get('key');

    if (keyField === modelKey) {
      jQuery(view.el).find('.nf-element').val(uniqueKey).trigger('change'); //console.log(jQuery( view.el ).find( '.nf-element' ).val())
    }
  }
});

/***/ }),

/***/ "./virtualgames-forms/src/scripts/ninja/password.jsx":
/*!***********************************************************!*\
  !*** ./virtualgames-forms/src/scripts/ninja/password.jsx ***!
  \***********************************************************/
/***/ (function() {

jQuery(document).ready(function ($) {
  new ninjaFormsPasswordController();
});
var requirements = [{
  regex: /^.*(?=.{8,}).*$/,
  message: 'at least 8 characters'
}, {
  regex: /^.*(?=.*[A-Z]).*$/,
  message: 'at least one capital letter'
}, {
  regex: /^.*(?=.*[a-z]).*$/,
  message: 'at least one lower-case letter'
}, {
  regex: /^.*(?=.*\d).*$/,
  message: 'at least one digit'
}, {
  regex: /^.*(?=.*[!@#$%^&*()\-_=+{};:,<.>]).*$/,
  message: 'at least one special symbol like from this set: !@#$%^&*()\-_=+{};:,<.>}'
}];
var errorID = 'invalid-password'; //console.log(dateFields)

var ninjaFormsPasswordController = Marionette.Object.extend({
  initialize: function initialize() {
    var radioFields = nfRadio.channel('fields');
    this.listenTo(radioFields, 'keyup:field', this.onBlurField);
    this.listenTo(radioFields, 'change:modelValue', this.onChangeModelValue);
  },
  onChangeModelValue: function onChangeModelValue(model) {
    var value = model.get('value');
    var fieldID = model.get('id');
    var type = model.get('type');
    var key = model.get('key');
    this.passwordChange(value, fieldID, type, key);
  },
  onBlurField: function onBlurField(el, model) {
    var value = jQuery(el).val();
    var fieldID = model.get('id');
    var type = model.get('type');
    var key = model.get('key');
    this.passwordChange(value, fieldID, type, key);
  },
  passwordChange: function passwordChange(value, fieldID, type, key) {
    //console.log(type)
    if (type === 'jg_password' && key === 'jg_password') {
      if (0 < value.length) {
        var message = [];
        var valid = true;
        jQuery.each(requirements, function (index) {
          // Loop through all regex password requirements
          var req = requirements[index]; //console.log( req )

          if (req.regex.test(value)) {
            message.push("<li class=\"valid\">".concat(req.message, "</li>"));
          } else {
            message.push("<li class=\"invalid\">".concat(req.message, "</li>"));
            valid = false;
          }
        });
        nfRadio.channel('fields').request('remove:error', fieldID, errorID);

        if (!valid) {
          // Remove error if all requirements are met
          var fieldModel = nfRadio.channel('fields').request('get:field', fieldID);
          var formModel = nfRadio.channel('app').request('get:form', fieldModel.get('formID'));
          message = "<ul class=\"password-requirements\">".concat(message.join(''), "</ul>");
          nfRadio.channel('fields').request('add:error', fieldID, errorID, message);
        }
      } else {
        // Remove error if field is empty
        nfRadio.channel('fields').request('remove:error', fieldID, errorID);
      }
    }
  },
  removeError: function removeError(el, model) {
    jQuery.each(requirements, function (index) {});
  }
});

/***/ }),

/***/ "./virtualgames-forms/src/scripts/ninja/profile.jsx":
/*!**********************************************************!*\
  !*** ./virtualgames-forms/src/scripts/ninja/profile.jsx ***!
  \**********************************************************/
/***/ (function() {

jQuery(document).ready(function ($) {
  new ninjaFormsCustomController();
});
var age_field = false;
var age_field2 = false;
var age_field3 = false;
var fileModel = Backbone.Model.extend({
  id: 0,
  name: '',
  tmpName: '',
  fieldID: 0
});
var FileCollection = Backbone.Collection.extend({
  model: fileModel
});
var fileView = Marionette.ItemView.extend({
  tagName: 'nf-section',
  template: '#tmpl-nf-field-file-row',
  events: {
    'click .delete': 'clickDelete'
  },
  clickDelete: function clickDelete(event) {
    nfRadio.channel('file_upload').trigger('click:deleteFile', event, this.model);
  }
});
var fileCollectionView = Marionette.CollectionView.extend({
  childView: fileView
});
var ninjaFormsCustomController = Marionette.Object.extend({
  initialize: function initialize() {
    var fieldsChannel = Backbone.Radio.channel('fields');
    this.listenTo(nfRadio.channel('fields'), 'render:view', this.setAgeField);
    this.listenTo(fieldsChannel, 'change:modelValue', this.calculateAge);
    this.listenTo(nfRadio.channel('fields'), 'change:field', this.removeError);
  },
  setAgeField: function setAgeField(view) {
    if ('age' === view.model.get('key')) age_field = jQuery(view.el).find('.nf-element');
    if ('age2' === view.model.get('key')) age_field2 = jQuery(view.el).find('.nf-element');
    if ('age3' === view.model.get('key')) age_field3 = jQuery(view.el).find('.nf-element');
  },
  calculateAge: function calculateAge(model) {
    if ('dob' === model.get('key') && age_field) {
      var date = model.get('value');
      var difference = new Date(new Date() - new Date(date)).getFullYear() - 1970;
      age_field.val(difference).trigger('change');
    }

    if ('dob2' === model.get('key') && age_field2) {
      var date = model.get('value');
      var difference = new Date(new Date() - new Date(date)).getFullYear() - 1970;
      age_field2.val(difference).trigger('change');
    }

    if ('dob3' === model.get('key') && age_field3) {
      var date = model.get('value');
      var difference = new Date(new Date() - new Date(date)).getFullYear() - 1970;
      age_field3.val(difference).trigger('change');
    }
  },
  removeError: function removeError(el, model) {
    nfRadio.channel('fields').request('remove:error', model.get('id'), 'jg-user-management');
  }
});

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/regex.js":
/*!*****************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/regex.js ***!
  \*****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (/^(?:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|00000000-0000-0000-0000-000000000000)$/i);

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/rng.js":
/*!***************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/rng.js ***!
  \***************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ rng; }
/* harmony export */ });
// Unique ID creation requires a high quality random # generator. In the browser we therefore
// require the crypto API and do not support built-in fallback to lower quality random number
// generators (like Math.random()).
var getRandomValues;
var rnds8 = new Uint8Array(16);
function rng() {
  // lazy load so that environments that need to polyfill have a chance to do so
  if (!getRandomValues) {
    // getRandomValues needs to be invoked in a context where "this" is a Crypto implementation. Also,
    // find the complete implementation of crypto (msCrypto) on IE11.
    getRandomValues = typeof crypto !== 'undefined' && crypto.getRandomValues && crypto.getRandomValues.bind(crypto) || typeof msCrypto !== 'undefined' && typeof msCrypto.getRandomValues === 'function' && msCrypto.getRandomValues.bind(msCrypto);

    if (!getRandomValues) {
      throw new Error('crypto.getRandomValues() not supported. See https://github.com/uuidjs/uuid#getrandomvalues-not-supported');
    }
  }

  return getRandomValues(rnds8);
}

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/stringify.js":
/*!*********************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/stringify.js ***!
  \*********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _validate_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./validate.js */ "./node_modules/uuid/dist/esm-browser/validate.js");

/**
 * Convert array of 16 byte values to UUID string format of the form:
 * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
 */

var byteToHex = [];

for (var i = 0; i < 256; ++i) {
  byteToHex.push((i + 0x100).toString(16).substr(1));
}

function stringify(arr) {
  var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  // Note: Be careful editing this code!  It's been tuned for performance
  // and works in ways you may not expect. See https://github.com/uuidjs/uuid/pull/434
  var uuid = (byteToHex[arr[offset + 0]] + byteToHex[arr[offset + 1]] + byteToHex[arr[offset + 2]] + byteToHex[arr[offset + 3]] + '-' + byteToHex[arr[offset + 4]] + byteToHex[arr[offset + 5]] + '-' + byteToHex[arr[offset + 6]] + byteToHex[arr[offset + 7]] + '-' + byteToHex[arr[offset + 8]] + byteToHex[arr[offset + 9]] + '-' + byteToHex[arr[offset + 10]] + byteToHex[arr[offset + 11]] + byteToHex[arr[offset + 12]] + byteToHex[arr[offset + 13]] + byteToHex[arr[offset + 14]] + byteToHex[arr[offset + 15]]).toLowerCase(); // Consistency check for valid UUID.  If this throws, it's likely due to one
  // of the following:
  // - One or more input array values don't map to a hex octet (leading to
  // "undefined" in the uuid)
  // - Invalid input values for the RFC `version` or `variant` fields

  if (!(0,_validate_js__WEBPACK_IMPORTED_MODULE_0__["default"])(uuid)) {
    throw TypeError('Stringified UUID is invalid');
  }

  return uuid;
}

/* harmony default export */ __webpack_exports__["default"] = (stringify);

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/v4.js":
/*!**************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/v4.js ***!
  \**************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _rng_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./rng.js */ "./node_modules/uuid/dist/esm-browser/rng.js");
/* harmony import */ var _stringify_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./stringify.js */ "./node_modules/uuid/dist/esm-browser/stringify.js");



function v4(options, buf, offset) {
  options = options || {};
  var rnds = options.random || (options.rng || _rng_js__WEBPACK_IMPORTED_MODULE_0__["default"])(); // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`

  rnds[6] = rnds[6] & 0x0f | 0x40;
  rnds[8] = rnds[8] & 0x3f | 0x80; // Copy bytes to buffer, if provided

  if (buf) {
    offset = offset || 0;

    for (var i = 0; i < 16; ++i) {
      buf[offset + i] = rnds[i];
    }

    return buf;
  }

  return (0,_stringify_js__WEBPACK_IMPORTED_MODULE_1__["default"])(rnds);
}

/* harmony default export */ __webpack_exports__["default"] = (v4);

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/validate.js":
/*!********************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/validate.js ***!
  \********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _regex_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./regex.js */ "./node_modules/uuid/dist/esm-browser/regex.js");


function validate(uuid) {
  return typeof uuid === 'string' && _regex_js__WEBPACK_IMPORTED_MODULE_0__["default"].test(uuid);
}

/* harmony default export */ __webpack_exports__["default"] = (validate);

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
!function() {
"use strict";
/*!**************************************************!*\
  !*** ./virtualgames-forms/src/scripts/ninja.jsx ***!
  \**************************************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ninja_activity__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ninja/activity */ "./virtualgames-forms/src/scripts/ninja/activity.jsx");
/* harmony import */ var _ninja_activity__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_ninja_activity__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ninja_errors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ninja/errors */ "./virtualgames-forms/src/scripts/ninja/errors.jsx");
/* harmony import */ var _ninja_errors__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_ninja_errors__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ninja_key__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ninja/key */ "./virtualgames-forms/src/scripts/ninja/key.jsx");
/* harmony import */ var _ninja_password__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ninja/password */ "./virtualgames-forms/src/scripts/ninja/password.jsx");
/* harmony import */ var _ninja_password__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_ninja_password__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ninja_profile__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ninja/profile */ "./virtualgames-forms/src/scripts/ninja/profile.jsx");
/* harmony import */ var _ninja_profile__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_ninja_profile__WEBPACK_IMPORTED_MODULE_4__);

 //import './ninja/age'
//import './ninja/emailConfirmProfile'



 //import './ninja/uploads'
}();
/******/ })()
;