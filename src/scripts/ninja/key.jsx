import { v4 as uuidv4 } from 'uuid';

jQuery( document ).ready( function( $ ) {
    new ninjaFormsUniqueKey()
})

var keyField = 'registration_key'
let uniqueKey = uuidv4();

var forms = false;
var ninjaFormsUniqueKey = Marionette.Object.extend({
    initialize: function() {
        let radioFields = nfRadio.channel( 'fields' )

        this.listenTo( radioFields, 'render:view', this.setValue )
    },   

    setValue: function( view ) {
        let modelKey = view.model.get( 'key' )

        if ( keyField === modelKey ) {
            jQuery( view.el ).find( '.nf-element' ).val( uniqueKey ).trigger( 'change' );
            //console.log(jQuery( view.el ).find( '.nf-element' ).val())
        }
    },
});
