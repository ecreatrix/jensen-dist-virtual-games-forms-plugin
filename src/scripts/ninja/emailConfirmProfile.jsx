jQuery( document ).ready( function( $ ) {
    new ninjaFormsEmailConfirmController()
})

var email = { key: 'email_1625951020517', field: false }
var emailConfirm = { key: 'email_confirmation_1625951090593', field: false }
var userID = false

var ninjaFormsEmailConfirmController = Marionette.Object.extend({
    initialize: function() {
        let radioFields = nfRadio.channel( 'fields' )

        this.listenTo( radioFields, 'render:view', this.setConfirmFields )

        this.listenTo( radioFields, 'blur:field', this.updateEmailConfirm )
        this.listenTo( radioFields, 'change:field', this.updateEmailConfirm )
        this.listenTo( radioFields, 'keyup:field', this.updateEmailConfirm )
    },

    setConfirmFields: function( view ) {
        let key = view.model.get( 'key' )

        if ( email.key === key ) 
            email.field = jQuery( view.el ).find( '.nf-element' )
        if ( emailConfirm.key === key ) 
            emailConfirm.field = jQuery( view.el ).find( '.nf-element' )
        if ( 'query_user_id' === key ) 
            userID = jQuery( view.el ).find( '.nf-element' ).val()
    },

    // Match email field when used on profile page by logged in users
    updateEmailConfirm: function( el, model ) {
        let key = model.get( 'key' )
        let value = model.get( 'value' )

        if( userID && email.key === key ) {
            emailConfirm.field.val( value ).trigger( 'change' )
        }
    },
});