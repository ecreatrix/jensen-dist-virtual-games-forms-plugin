jQuery( document ).ready( function( $ ) {
    new ninjaFormsVersionController()
})

var submitSportIDs
var token = 'jg-user-management'

var ninjaFormsVersionController = Marionette.Object.extend({
    initialize: function() {
        var fieldsChannel = nfRadio.channel( 'fields' )

        this.listenTo( fieldsChannel, 'render:view', this.setVersion )
    },

    setVersion: function( view ) {
        let model = view.model
        let key = model.get( 'key' )
        let formID = model.get( 'formID' )

        if ( 'version' === key ) {
            var currentFormVersion = 'main'
            if( formID.indexOf( '_' ) >= 0 ) {
                currentFormVersion = formID.substr( formID.indexOf('_') )
            }

            jQuery( view.el ).find( '.nf-element' ).val( currentFormVersion ).trigger( 'change' );
        }
    },
});