jQuery( document ).ready( function( $ ) {
    new ninjaFormsCustomController();
});

var age_field = false;
var age_field2 = false;
var age_field3 = false;

var fileModel = Backbone.Model.extend( {
    id: 0,
    name: '',
    tmpName: '',
    fieldID: 0
} );

var FileCollection = Backbone.Collection.extend( {
    model: fileModel
} );

var fileView = Marionette.ItemView.extend( {
    tagName: 'nf-section',
    template: '#tmpl-nf-field-file-row',

    events: {
        'click .delete': 'clickDelete'
    },

    clickDelete: function( event ) {
        nfRadio.channel( 'file_upload' ).trigger( 'click:deleteFile', event, this.model );
    }

} );

var fileCollectionView = Marionette.CollectionView.extend( {
    childView: fileView
} );

var ninjaFormsCustomController = Marionette.Object.extend({
    initialize: function() {
        var fieldsChannel = Backbone.Radio.channel( 'fields' );

        this.listenTo( nfRadio.channel( 'fields' ), 'render:view', this.setAgeField );
        this.listenTo( fieldsChannel, 'change:modelValue', this.calculateAge );
        this.listenTo( nfRadio.channel( 'fields' ), 'change:field', this.removeError );
    },

    setAgeField: function( view ) {
        if ( 'age' === view.model.get( 'key' ) ) age_field = jQuery( view.el ).find( '.nf-element' );
        
        if ( 'age2' === view.model.get( 'key' ) ) age_field2 = jQuery( view.el ).find( '.nf-element' );
        
        if ( 'age3' === view.model.get( 'key' ) ) age_field3 = jQuery( view.el ).find( '.nf-element' );
    },
    
    calculateAge: function( model ) {
        if ( 'dob' === model.get( 'key' ) && age_field ) {
            var date = model.get( 'value' );
            var difference = new Date(new Date() - new Date(date)).getFullYear() - 1970;
            age_field.val( difference ).trigger( 'change' );
        }

        if ( 'dob2' === model.get( 'key' ) && age_field2 ) {
            var date = model.get( 'value' );
            var difference = new Date(new Date() - new Date(date)).getFullYear() - 1970;
            age_field2.val( difference ).trigger( 'change' );
        }

        if ( 'dob3' === model.get( 'key' ) && age_field3 ) {
            var date = model.get( 'value' );
            var difference = new Date(new Date() - new Date(date)).getFullYear() - 1970;
            age_field3.val( difference ).trigger( 'change' );
        }
    },

    removeError: function( el, model ) {
        nfRadio.channel( 'fields' ).request('remove:error', model.get( 'id' ), 'jg-user-management');
    }
});