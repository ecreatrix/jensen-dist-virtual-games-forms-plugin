jQuery( document ).ready( function( $ ) {
    new ninjaFormsUploadsType()
})

var uploadTypeField = { key: 'upload_type_1634666094036', field: false }
var pointsField = { key: 'points_1634666066128', field: false }

var forms = false
var ninjaFormsUploadsType = Marionette.Object.extend({
    initialize: function() {
        let radioFields = nfRadio.channel( 'fields' )

        this.listenTo( radioFields, 'render:view', this.setTypeField )

        this.listenTo( radioFields, 'change:modelValue', this.onChangeModelValue )
    },   

    setTypeField: function( view ) {
        let key = view.model.get( 'key' )

        if ( uploadTypeField.key === key ) 
            uploadTypeField.field = jQuery( view.el ).find( '.nf-element' )

        if ( pointsField.key === key ) 
            pointsField.field = jQuery( view.el ).find( '.nf-element' )
    },

    onChangeModelValue: function( model ) {
        var fieldID = model.get( 'id' )
        var formID = model.get( 'formID' )
        var field = $('#nf-field-' + fieldID + '-container')
        var form = $('.form-' + formID)

        if( !forms ) {
            forms = form
        }
          
        let uploadType, uploadLabel
        var points = 100

        if ( forms && fieldID.indexOf("_1") >= 0) {
            form = forms[1]
            uploadType = 'challenge-2'
            uploadLabel = 'Challenge 2 Results'
            //console.log((uploadTypeField.field[1]).val('test'))
            
            if (uploadType && uploadTypeField.field[1]) {
                uploadTypeField.field[1].val(uploadType).trigger('change')
            }
            if (points && pointsField.field[1]) {
                pointsField.field[1].val(points).trigger('change')
            }
        } else if ( forms && fieldID.indexOf("_2") >= 0) {
            form = forms[2]
            uploadType = 'challenge-3'
            uploadLabel = 'Challenge 3 Results'
            
            if (uploadType && uploadTypeField.field[2]) {
                uploadTypeField.field[2].val(uploadType).trigger('change')
            }
            if (points && pointsField.field[2]) {
                pointsField.field[2].val(points).trigger('change')
            }
        } else if ( forms && fieldID.indexOf("_3") >= 0) {
            form = forms[3]
            uploadType = 'challenge-4'
            uploadLabel = 'Challenge 4 Results'
                //console.log(uploadTypeField.field)
            
            if (uploadType && uploadTypeField.field[3]) {
                //console.log(uploadTypeField.field[3])
                uploadTypeField.field[3].val(uploadType).trigger('change')
            }
            if (points && pointsField.field[3]) {
                pointsField.field[3].val(points).trigger('change')
            }
        } else {
            form = forms[0]
            uploadType = 'challenge-1'
            uploadLabel = 'Challenge 1 Results'
            
            if (uploadType && uploadTypeField.field[0]) {
                uploadTypeField.field[0].val(uploadType).trigger('change')
            }
            if (points && pointsField.field[0]) {
                pointsField.field[0].val(points).trigger('change')
            }
        }
    },
});