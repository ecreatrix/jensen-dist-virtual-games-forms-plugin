jQuery( document ).ready( function( $ ) {
    new ninjaFormsCustomController()
})

var submitSportIDs
var token = 'jg-forms'

var ninjaFormsCustomController = Marionette.Object.extend({
    initialize: function() {
        var fieldsChannel = nfRadio.channel( 'fields' )
        
        this.listenTo( fieldsChannel, 'blur:field', this.removeErrorFields )
        this.listenTo( fieldsChannel, 'change:field', this.removeErrorFields )
        this.listenTo( fieldsChannel, 'keyup:field', this.removeErrorFields )
    },

    removeErrorFields: function( el, model ) {
        nfRadio.channel( 'fields' ).request( 'remove:error', model.get( 'id' ), token)
    },
});