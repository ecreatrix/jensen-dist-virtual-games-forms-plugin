jQuery( document ).ready( function( $ ) {
    new ninjaFormsAgeController()
})

var dateFields = {
    main: { key: 'dob', field: false },
    child1: { key: 'child_1_dob', field: false },
    child2: { key: 'child_2_dob', field: false },
    child3: { key: 'child_3_dob', field: false },
    child4: { key: 'child_4_dob', field: false },
}
let errorID = 'invalid-age'
let error_profile_message = ''
let error_child_message = ''

var ninjaFormsAgeController = Marionette.Object.extend({
    initialize: function() {
        let radioFields = nfRadio.channel( 'fields' )

        this.listenTo( radioFields, 'render:view', this.setAgeFields )

        this.listenTo( radioFields, 'keyup:field', this.onBlurField )
        this.listenTo( radioFields, 'change:modelValue', this.onChangeModelValue )
    },

    setAgeFields: function( view ) {
        let type = view.model.get( 'type' )
        let key = view.model.get( 'key' )
        let container_class = view.model.get( 'container_class' )
        let value = view.model.get( 'value' )

        if( 'html' === type && ~container_class.indexOf( 'error-profile-message')  ) {
            error_profile_message = value
        } else if( 'html' === type && ~container_class.indexOf( 'error-youth-message')  ) {
            error_child_message = value
        }

        jQuery.each ( dateFields, function( index ) {
            let element = dateFields[index]

            if ( element.key === key ) 
                dateFields[index].field = jQuery( view.el ).find( '.nf-element' )
        } )
    },

    onChangeModelValue: function( model ) {
        var type = model.get( 'type' )

        if(type === 'date') {
            var date = model.get( 'value' )
            var fieldID = model.get( 'id' )
            var category = model.get( 'container_class' )

            this.dateChange( date, fieldID, category )
        }
    },

    onBlurField: function( el, model ) {
        var type = model.get( 'type' )

        if(type === 'date') {
            var date = model.get( 'value' )
            var fieldID = model.get( 'id' )
            var category = model.get( 'container_class' )

            this.dateChange( date, fieldID, category )
        }
    },

    dateChange: function( date, fieldID, category ) {
        if( 0 < date.length ) {
            var difference = new Date(new Date() - new Date(date)).getFullYear() - 1970
            //console.log(difference)

            if( difference >= 10 && difference <= 19 ) {
                nfRadio.channel( 'fields' ).request( 'remove:error', fieldID, errorID )
            } else {
                if( 'profile' === category ) {
                    var fieldModel = nfRadio.channel( 'fields' ).request( 'get:field', fieldID )
                    var formModel  = nfRadio.channel( 'app' ).request( 'get:form',  fieldModel.get( 'formID' ) )

                    nfRadio.channel( 'fields' ).request( 'add:error', fieldID, errorID, error_profile_message )
                } else {
                    var fieldModel = nfRadio.channel( 'fields' ).request( 'get:field', fieldID )
                    var formModel  = nfRadio.channel( 'app' ).request( 'get:form',  fieldModel.get( 'formID' ) )

                    nfRadio.channel( 'fields' ).request( 'add:error', fieldID, errorID, error_child_message )
                }
            }
        }
    },
});